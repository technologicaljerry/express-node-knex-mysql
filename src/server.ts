import express from 'express';

const app = express();

const HOST = 'localhost';
const PORT = process.env.SERVER_PORT;

app.listen(PORT, () => {
   console.log(`Server is running on host:${HOST} port:${PORT}`);
});